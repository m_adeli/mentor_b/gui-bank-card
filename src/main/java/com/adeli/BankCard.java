package com.adeli;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class BankCard {
    static Map<String, String> banks = new HashMap<>();

    public static String getBankName(String cardNumber) {
        String card = String.join("", cardNumber.split("-"));
        String bankCode = card.substring(0, 6);
        return banks.get(bankCode);
    }

    public static void main(String[] args) {
        banks.put("622106", "Parsian");
        banks.put("505785", "Iran Zamin");
        banks.put("627381", "Ansar");
        banks.put("627412", "Eqtesad Novin");
        banks.put("502229", "Pasargad");
        banks.put("639347", "Pasargad");
        banks.put("639194", "Parsian");
        banks.put("627648", "Tose-e Saderat Iran");
        banks.put("627884", "Tose-e Taavon");
        banks.put("627353", "Tejarat");
        banks.put("636214", "Ayandeh");

        JFrame frame = new JFrame("Bank Card");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JLabel label;
        JFormattedTextField input = null;
        JButton button = null;
        JPanel panel;
        MaskFormatter formatter;

        BoxLayout layout = new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS);
        frame.setLayout(layout);

        try {
            label = new JLabel("Bank Card");
            button = new JButton("Search");
            formatter = new MaskFormatter("####-####-####-####");
            input = new JFormattedTextField(formatter);
            input.setColumns(20);
            panel = new JPanel();
            panel.add(label);
            panel.add(input);
            panel.add(button);
            frame.add(panel);
        } catch (ParseException e) {
            System.err.println("Unable to add Bank Card");
        }
        frame.pack();
        frame.setVisible(true);

        button.addActionListener(e -> {
            JButton btn = (JButton) e.getSource();
            JPanel pnl = (JPanel) btn.getParent();
            for (Component com : pnl.getComponents()) {
                if (com instanceof JFormattedTextField) {
                    JFormattedTextField txt = (JFormattedTextField) com;
                    String cardNumber = txt.getText();
                    JOptionPane.showMessageDialog(frame, getBankName(cardNumber));
                }
            }
        });
    }
}